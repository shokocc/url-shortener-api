# URL Shortener - API

A simple url shortener

Postman reference:

> https://www.getpostman.com/collections/73b6938a8257e4bf69a0

Setup:

> npm install

> npm run migration:up

> npm run migration:seed

> npm start