require('mandatoryenv').load([
    'DB_HOST',
    'DB_DATABASE',
    'DB_USER',
    'DB_PASSWORD'
]);

const mysqlm = require('mysqlm');

const config = {
    host: env.DB_HOST,
    database: env.DB_DATABASE,
    user: env.DB_USER,
    password: env.DB_PASSWORD
}

module.exports = {config, mysqlm}