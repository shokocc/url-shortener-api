const {config, mysqlm} = require('./.common.js');

async function up () {
  const {query} = mysqlm.connect(config);

  await query(`CREATE TABLE TEST();`);
}

async function down () {
  const {query} = mysqlm.connect(config);

  await query(`DROP TABLE TEST;`);
}

module.exports = { up, down }