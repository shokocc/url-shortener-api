const {config, mysqlm} = require('./.common.js');

async function up () {
  const {query} = mysqlm.connect(config);

  await query(`
    CREATE TABLE \`users\`(
      id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
      
      username VARCHAR(255) UNIQUE,
      password VARCHAR(255),
      roles_id BIGINT UNSIGNED,
      status BIT DEFAULT 1,
      
      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    )
  `);
}

async function down () {
  const {query} = mysqlm.connect(config);
  await query(`DROP TABLE \`users\``);
}

module.exports = { up, down }