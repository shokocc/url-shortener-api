const {config, mysqlm} = require('./.common.js');

async function up () {
  const {query} = mysqlm.connect(config);

  await query(`
  CREATE TABLE links(
    id BIGINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    
    code TEXT,
    url TEXT,
    status BIT DEFAULT 1,
    clicks BIGINT UNSIGNED DEFAULT 0,
    
    users_id BIGINT UNSIGNED,

    FOREIGN KEY (users_id) REFERENCES users(id),
    
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL DEFAULT NULL
  );
  `);
}

async function down () {
  const {query} = mysqlm.connect(config);

  await query(`DROP TABLE links;`);
}

module.exports = { up, down }