

const {mysqlm, config} = require('../migrations/.common.js');
const conn = mysqlm.connect(config);

module.exports = {
    tables: () => [
        { table: 'roles', data: require('./roles.js') },
        { table: 'users', data: require('./users.js') },
    ],
    process: ({table, data}) => {
        if(typeof data === 'string') {
            return conn.query(data);
        } else {
            return conn.query(`INSERT INTO \`${table}\` SET ?`, data);
        }
    }
}