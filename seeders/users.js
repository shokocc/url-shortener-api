require('mandatoryenv').load({
  'ADMIN_PASSWORD': 'admin'
});

const encrypt = (text) => require('crypto').createHash('sha256').update(text).digest('hex');

const pass = process.env.ADMIN_PASSWORD;

module.exports = [
  { id: 1, username: "admin", password: encrypt(pass), roles_id: 1, status: 1 }
];