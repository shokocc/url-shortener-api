const linksModel = require('../models/links');
const shortid = require('shortid');

async function generateCode() {
    for (let i = 0; i < 10; i++) {
        const code = shortid();
        
        const result = await linksModel.getByCode(code);

        if(!result) {
            return code;
        }
    }
    throw 'Couldnt generate unique code in 10 attempts';
}

async function checkOwnership(links_id, users_id) {
    const result = await linksModel.getById(links_id);

    if(!result) {
        return false;
    }

    return (result.users_id == users_id);
}

module.exports = {
    options: linksModel.options,

    async getByCode(req, res) {
        let temp = await linksModel.getByCode(req.params.code);
        
        if(!temp) {
            return res.status(400).json({status: false, message: 'Found 0 links matching that code'});
        }

        if(temp.status != true) {
            return res.status(400).json({status: false, message: 'Requested link code is disabled'});
        }

        await linksModel.incrementViews(temp.id);

        let result = await linksModel.getById(temp.id);

        res.json({
            status: true,
            data: result
        })
    },

    async create(req, res) {
        const code = await generateCode();

        let linksInput = {
            code: code,
            url: req.body.url,
            users_id: req.token.users_id
        }

        let result = await linksModel.create(linksInput);
        
        res.json({
            status: true,
            data: result
        })
    },
    
    async update(req, res) {
        let userOwnLink = await checkOwnership(req.body.id, req.token.users_id);

        if(!userOwnLink) {
            return res.status(401).json({status: false, message: 'You dont have the ownership of this link'});
        }

        let result = await linksModel.update({id: req.body.id, status: req.body.status});
        
        res.json({
            status: true,
            data: result
        })
    },
    
    async delete(req, res) {
        let userOwnLink = await checkOwnership(req.body.id, req.token.users_id);

        if(!userOwnLink) {
            return res.status(401).json({status: false, message: 'You dont have the ownership of this link'});
        }

        let result = await linksModel.delete(req.body.id);
        
        res.json({
            status: true,
            message: result ? 'Deleted' : 'Already deleted'
        })
    },
    
    async getDataTable(req, res){
        let response = await linksModel.datatable(req, {
            users_id: req.token.users_id
        });   
        
        res.json({
            status: true,
            message: "Returning data",
            ...response
        })
    },
}
