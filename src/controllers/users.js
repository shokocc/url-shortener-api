const usersModel = require('../models/users.js');

const oauth2 = require('../middlewares/oauth2.js');

const aobj = require('aobj');

module.exports = {
    options: usersModel.options,

    async getAll(req, res) {
        let result = await usersModel.getAll();
        
        res.json({
            status: true,
            data: result
        })
    },

    async getDataTable(req, res){
        const response = await usersModel.datatable(req);
        
        res.json({
            status: true,
            message: "Returning data",
            ...response
        })
    },

    async login(req, res){
        let userInput = aobj.extract(req.body, ['username', 'password']);
        
        const loginResult = await oauth2.validateUser(userInput);

        if(loginResult.status !== true) {
            return res.status(400).json(loginResult);
        }

        res.json({
            status: true,
            message: 'Login successfully, returning access token',
            access_token: loginResult.access_token,
            data: {
                users_id: loginResult.data.id,
                roles_id: loginResult.roles_id,
                role: loginResult.role
            }
        });
    },

    async tokenStatus(req, res){
        let access_token = req.body.access_token;
        
        const tokenVerification = await oauth2.tokenStatus(access_token);

        if(tokenVerification.status !== true) {
            return res.status(401).json(tokenVerification);
        }

        const {loginResult} = tokenVerification;

        res.json({
            status: true,
            message: 'Token is ok, returning access token and data',
            access_token: loginResult.access_token,
            data: {
                users_id: loginResult.data.id,
                roles_id: loginResult.roles_id,
                role: loginResult.role
            }
        });
    },

    async setStatus(req, res){
            let userInput = aobj.extract(req.body, ['id', 'status']);

            userInput.status = !!userInput.status;

            const user = await usersModel.update(userInput);

            let newStatus = user.status ? 'Enabled' : 'Disabled';
            
            res.json({ status: true, message: `User status set to ${newStatus}` });
    },

    async register(req, res){
        let _users_id;
        try {
            let userInput = aobj.extract(req.body, ['username', 'password']);
            userInput.password = oauth2.encrypt(userInput.password);

            const user = await usersModel.create({
                ...userInput,
                roles_id: oauth2.role['User'],
                status: 1
            });
            
            res.json({status: true, message: 'Registered successfully, please log in'});
        } catch (error) {
            logger.error(error);

            if(_users_id) {
                usersModel.update({id: _users_id, status: false})
                .then(() => logger.info(`User ${_users_id} set to inactive because of failed register attempt`))
                .catch(err => logger.error(err))
            }

            res.json({status: false, message: 'Register failed'});
        }
    }

}
