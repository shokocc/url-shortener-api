const aobj = require('aobj');

function cleanMeta(obj) {
    delete obj.current_page;
    delete obj.from;
    delete obj.last_page;
    delete obj.path;
    delete obj.per_page;
    delete obj.to;
    delete obj.showing;
    delete obj.total;
    
    return obj;
}

function _paginate(url, start, length, search, count, showing, meta = {fields_map: {}}) {
    length = parseInt(length);
    start = parseInt(start);
    let newUrl = (env.BASE_URL || '') + url + '?';
    if(search.data){
        newUrl += '?search[data]=' + search.data + '&'
    }
    let current_page = start / length + 1;
    let last_page = Math.ceil(count / length);
    
    return {
        links: {
            first: newUrl + 'page=1',
            last: newUrl + 'page=' + last_page,
            prev: current_page > 1 ? newUrl + 'page=' + (current_page - 1) : '',
            next: current_page < last_page ? newUrl + 'page=' + (current_page + 1) : '',
        },
        meta: {
            current_page,
            from: start,
            last_page,
            path: (env.BASE_URL || '') + url,
            per_page: length,
            to: length,
            showing,
            total: count,
            ...cleanMeta(meta)
        }
    };
}

function onRequest(request) {
    return {
        async byFields(fields = [], cb = async ({fields, search, start, length, order, meta}) => {
            return {
                count: 0,
                data: []
            }
        }) {
            if (!fields) throw 'Fields cant be an empty Array []';
            if (!fields.length) throw 'Fields cant be an empty Array []';
            if (fields.length <= 0) throw 'Fields cant be an empty Array []';

            const url = request.originalUrl.split('?')[0];

            let {
                search = {column: '', data: ''},
                start = 0,
                length = 10,
                order = [{column: 0, dir: 'asc'}],
                meta = {fields_map: {}}
            } = request.query;
            
            if(!search.column) search.column = '';
            if(!search.data) search.data = '';
            if(!meta.fields_map) meta.fields_map = {};
            
            if(Object.keys(meta.fields_map).length === 0) {
                meta.fields_map = aobj.invert(fields);
            }
            if(!search.column) {
                search.column = fields[0];
            }
            
            order = order.map(v => {
                try {
                    if(v.column)
                        v.column = (aobj.invert(meta)[v.column]) || v.column;
                    return v;
                } catch (error) {}
            })
            
            let client_fields = aobj.invert(meta.fields_map);
            
            const { count, data } = await Promise.resolve(cb({
                fields,
                search,
                start,
                length,
                order: {column: client_fields[order[0].column] || 'id', dir: order[0].dir || 'ASC'},
                meta
            }));
            
            const paginate = _paginate(url, start, length, search, count, data.length, meta);
            
            return {
                ...paginate,
                data
            };
        }
    }
}

function decorateSearch(query, fields, searchValue='') {
    query = query.andOpenBrackets();
    for (const field of fields)
        query = query.or(field, 'LIKE', '%'+(searchValue || '')+'%');
    query = query.closeBrackets();
    return query;
}

module.exports = {
    onRequest,
    decorateSearch
};