const aobj = require('aobj');

module.exports = (options = {body: [], params: [], query: []}) => {
    return (req, res, next) => {
        if (aobj.has(req.body, options.body) !== true)
            throw { code: 400, message: 'Request body must have: '+options.body.join(', ') };
        if (aobj.has(req.params, options.params) !== true)
            throw { code: 400, message: 'Request params must have: '+options.params.join(', ') };
        if (aobj.has(req.query, options.query) !== true)
            throw { code: 400, message: 'Request url must have: '+options.query.join(', ') };
        next();
    }
};