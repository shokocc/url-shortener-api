const jwt = require('jsonwebtoken');
const aobj = require('aobj');

const usersModel = require('../models/users.js');

const milliseconds = (h=0, m=0, s = 0) => ((h*60*60+m*60+s)*1000);

const encrypt = (text) => require('crypto').createHash('sha256').update(text).digest('hex');

function encodeToken(value) {
    return jwt.sign(value, env.SECRET);
}
function decodeToken(value) {
    try {
        return jwt.verify(value, env.SECRET);
    } catch (error) {
        logger.error(error);
        return null;
    }
}
function generateToken({users_id, username, password, roles_id}) {        
    let token = encodeToken({
        users_id,
        username,
        password,
        roles_id,
        expires_at: Date.now() + milliseconds(24, 00) // 24 Hours from now
    })
    
    return token;
}

const role = {
    'Admin': 1,
    'User': 2,
};

async function validateUser({username, password, skipPasswordEncryption= false}) {
    if(!skipPasswordEncryption) {
        password = encrypt(password);
    }

    const user = await usersModel.getByUsername(username);
    
    if(!user)
        return {status: false, message: 'User is not registered'};

    if(user.status !== true)
        return {status: false, message: 'User account is disabled'};

    if(password !== user.password)
        return {status: false, message: 'Invalid login attempt, password doesn\'t match'};
    
    let access_token = generateToken({
        users_id: user.id,
        username,
        password,
        roles_id: user.roles_id
    })

    return {
        status: true,
        data: user,
        access_token: access_token,
        roles_id: user.roles_id,
        role: aobj.invert(role)[user.roles_id]
    };
}

async function tokenStatus(access_token) {
    const token = decodeToken(access_token);

    if (!token){
        return {status: false, message: 'Authorization Token is not valid'};
    }

    if (Date.now() >= token.expires_at) {
        return {status: false, message: 'Authorization Token expired'};
    }

    let loginResult = await validateUser({...aobj.extract(token, ['username', 'password']), skipPasswordEncryption: true});
    
    if (loginResult.status !== true) {
        return {status: false, message: 'User account that generated this access token is disabled'};
    }

    return {status: true, token, access_token, loginResult};
}

module.exports = {
    role,
    encrypt,
    generateToken,
    validateUser,
    tokenStatus,
    authorize: (id_roles = ['*']) => async (req, res, next) => {
        if (!aobj.has(req.headers, 'authorization')) {
            return res.status(400).json({status: false, message: 'You didnt pass the header Authorization'});
        }

        const { authorization } = req.headers;

        if (!authorization.startsWith("Bearer ")) {
            return res.status(400).json({status: false, message: 'Cannot read Authorization header. Must be Bearer <token>'});
        }

        const access_token = authorization.substring(7, authorization.length);

        const tokenVerification = await tokenStatus(access_token);

        if(tokenVerification.status !== true) {
            return res.status(401).json(tokenVerification);
        }

        const {token, loginResult} = tokenVerification;
        
        const allowAnyRole = id_roles.includes('*');
        const allowUserRole = id_roles.includes(loginResult.roles_id);

        if(allowAnyRole || allowUserRole) {
            req.token = token;
            req.token.roles_id = loginResult.roles_id;
            req.token.role = aobj.invert(role)[loginResult.roles_id];
            return next();
        }

        res.status(401).json({status: false, message: 'Access denied, action not allowed'});
    }

}