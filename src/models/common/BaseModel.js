const mysqlm = require('mysqlm');
const aobj = require('aobj');

class RawValue {
    constructor(_value) {
        this.val = _value;
    }
    getValue() {
        return this.val;
    }
}

class baseChain {
    constructor(parent){
        this.escapeId = parent.escapeId;
        this.escape = parent.escape;
        this.onQuery = parent.onQuery;
        this.db = parent.db;
        
        this.type = parent.type;
        
        this.string_fields = parent.string_fields;
        this.pre_string_fields = parent.pre_string_fields;
        this.post_string_fields = parent.post_string_fields;
        
        this.query = parent.query;
        this.condition = parent.condition;
        this.order = parent.order;
        this.limits = parent.limits;
    }
    getQuery() {
        return ([
            this.type, 
            this.pre_string_fields, 
            this.string_fields, 
            this.post_string_fields, 
            this.query, 
            this.condition, 
            this.order, 
            this.limits
        ].map(v => v ? v.trim() : '').join(' ') + '').trim();
    }
    all() {
        let _query = this.getQuery();
        
        this.onQuery(_query);
        
        return Promise.resolve(this.db.query(_query));
    }
    one() {
        let _query = this.getQuery();
        
        this.onQuery(_query);
        
        return Promise.resolve(this.db.queryOne(_query));
    }
}

class limitChain extends baseChain{
    constructor(parent) {
        super(parent);
    }
    limit(_limit, _offset) {
        _limit = parseInt(_limit);
        _offset = parseInt(_offset);
        
        this.limits += ` LIMIT ${escape(_limit)}`;
        
        if(isNaN(_limit))
            _limit = null;
        if(isNaN(_offset))
            _offset = null;
    
        if(_offset !== null && _offset !== undefined)
            this.limits += ` OFFSET ${escape(_offset)}`;
        
        return new baseChain(this);
    }
}
class findChain extends limitChain{
    orderBy(field, _order = 'ASC' || 'DESC') {
        _order = _order.toUpperCase();
    
        if (!this.order) this.order += ` ORDER BY`;
        
        this.order += ` ${this.escapeId(field)} ${_order}`;
            
        return new limitChain(this);
    }
    andOpenBrackets() {
        this.condition += ` AND (`;
        return this;
    }
    orOpenBrackets() {
        this.condition += ` OR (`;
        return this;
    }
    closeBrackets() {
        this.condition += ` )`;
        return this;
    }
    and(field, operand, value) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` AND`;
        
        this.condition += ` ${this.escapeId(field)} ${operand} ${this.escape(value)}`;
        
        return this;
    }
    andNull(field) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` AND`;
        
        this.condition += ` ${this.escapeId(field)} IS NULL`;
            
        return this;
    }
    andNotNull(field) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` AND`;
        
        this.condition += ` ${this.escapeId(field)} IS NOT NULL`;
            
        return this;
    }
    or(field, operand, value) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` OR`;
        
        this.condition += ` ${this.escapeId(field)} ${operand} ${this.escape(value)}`;
            
        return this;
    }
    orNull(field) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` OR`;
        
        this.condition += ` ${this.escapeId(field)} IS NULL`;
            
        return this;
    }
    orNotNull(field) {
        if(!this.condition.endsWith('('))
        if (this.condition) this.condition += ` OR`;
        
        this.condition += ` ${this.escapeId(field)} IS NOT NULL`;
            
        return this;
    }
}

class joinChain {
    constructor(parent){
        this.escapeId = parent.escapeId;
        this.escape = parent.escape;
        this.onQuery = parent.onQuery;
        this.db = parent.db;
        
        this.type = parent.type;
        
        this.string_fields = parent.string_fields;
        this.pre_string_fields = parent.pre_string_fields;
        this.post_string_fields = parent.post_string_fields;
        
        this.query = parent.query;
        this.condition = parent.condition;
        this.order = parent.order;
        this.limits = parent.limits;
    }    
    on(_field, _operand, _value) {
        this.query += ` ON ${this.escapeId(_field)} ${_operand} ${this.escape(_value)}`;
        return new selectChain(this);
    }
}

class selectChain extends baseChain{
    constructor(parent) {
        super(parent);
    }
    innerJoin(table_b) {
        this.query += ` INNER JOIN ${this.escapeId(table_b)}`;
        return new joinChain(this);
    }

    leftJoin(table_b) {
        this.query += ` LEFT JOIN ${this.escapeId(table_b)}`;
        return new joinChain(this);
    }

    rightJoin(table_b) {
        this.query += ` RIGHT JOIN ${this.escapeId(table_b)}`;
        return new joinChain(this);
    }

    fullJoin(table_b) {
        this.query += ` FULL JOIN ${this.escapeId(table_b)}`;
        return new joinChain(this);
    }
    
    where(_field, _operand, _value) {
        if(arguments.length === 3)
            this.condition += ` WHERE ${this.escapeId(_field)} ${_operand} ${this.escape(_value)} `.trim();
        return new findChain(this);
    }

    whereNotNull(_field) {
        this.condition += ` WHERE ${this.escapeId(_field)} IS NOT NULL `.trim();
        return new findChain(this);
    }

    whereNull(_field) {
        this.condition += ` WHERE ${this.escapeId(_field)} IS NULL `.trim();
        return new findChain(this);
    }
    
    limit(_limit, _offset) {
        _limit = parseInt(_limit);
        _offset = parseInt(_offset);
        
        if(isNaN(_limit))
            _limit = null;
        
        if(isNaN(_offset))
            _offset = null;
        
        this.limits += ` LIMIT ${escape(_limit)}`;
        
        if(_offset !== null && _offset !== undefined)
            this.limits += ` OFFSET ${escape(_offset)}`;
        
        return new baseChain(this);
    }
}

class BaseModel {
    constructor({
        table_name = '',
        fields = [],
        primaryKey = '',
        control_fields = [],
        base_sql = '',
        connection = {},
        onQuery = (query) => {}
    }) {
        this.options = {
            table_name,
            fields,
            control_fields
        }
        this.db = connection;
        this.onQuery = onQuery;
        this.table_name = table_name;
        this.fields = [...control_fields, ...fields];
        
        if(base_sql === '' || base_sql === null || base_sql === undefined) {
            this.base_sql = this.escapeId(table_name);
        } else {
            this.base_sql = `(SELECT * FROM ${this.escapeId(this.table_name)}) as base`;
        }
    }
    
    escape(v) {
        return v instanceof RawValue ? v.getValue() : mysqlm.escape(v);
    }
    
    escapeId(v) {
        return v instanceof RawValue ? v.getValue() : mysqlm.escapeId(v);
    }
    
    select(field = '' || []) {
        let fields = Array.isArray(field) ? field : [field];
        
        this.string_fields = fields.map(v => this.escapeId(v)).join(', ');
        
        this.pre_string_fields = 'SELECT';
        this.post_string_fields = 'FROM '+this.table_name;
        this.query = '';
        this.condition = '';
        this.order = '';
        this.limits = '';
        
        return new selectChain(this);
    }
    
    getAll() {
        return this.select('*').all();
    }

    getById(id) {
        return this.select('*').where('id', '=', id).one();
    }

    async create(obj) {
        const { insertId } = await this.db.query(`INSERT INTO \`${this.table_name}\` SET ?`, obj);

        return await this.select('*').where('id', '=', insertId).one();
    }

    async createMany(objArr = []) {
        const promises = objArr.map(obj => this.db.query(`INSERT INTO \`${this.table_name}\` SET ?`, obj));

        const result = await Promise.all(promises);

        const result_ids = result.map(v => v.insertId);

        let id = result_ids.shift();

        let getCreatedQuery = this.select('*').where('id', '=', id);

        do {
            getCreatedQuery.or('id', '=', id);
        } while(id = result_ids.shift());

        return await getCreatedQuery.all();
    }

    async update(obj) {
        await this.db.query(`UPDATE \`${this.table_name}\` SET ? WHERE id = ?`, [obj, obj.id]);
        
        const result = await this.select('*').where('id', '=', obj.id).one();
        
        return result;
    }

    async delete(id) {
        const temp = await this.select('*').where('id', '=', id).one()
        
        if(!temp) // If doesnt exists
            return false;
        
        const r = await this.db.query(`DELETE FROM \`${this.table_name}\` WHERE id = ?`, id);
        
        return r.affectedRows > 0;
    }
    
    raw(value) {
        return new RawValue(value);
    }
}

module.exports = BaseModel;