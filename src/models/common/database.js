const mysqlm = require('mysqlm');

const connection = mysqlm.connect({
    host: env.DB_HOST,
    database: env.DB_DATABASE,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    typeCast( field, defaultTypeCast ) {
		if ( ( field.type === "BIT" ) && ( field.length === 1 ) ) {
			const bytes = field.buffer();
			return( bytes[ 0 ] === 1 );
		}
		return( defaultTypeCast() );
	}
})

module.exports = {...connection, mysqlm};