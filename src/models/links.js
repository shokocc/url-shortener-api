const dataTableSearch = require('../helpers/datatable.js');
const BaseModel = require('./common/BaseModel.js');
const db = require('./common/database.js');

const options = {
    table_name: 'links',
    connection: db,
    fields: [
        'id',
        'code',
        'url',
        'status',
        'clicks',
        'users_id',
    ],
    control_fields: [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ],
    //onQuery: (q => console.log(q))
}

class Links extends BaseModel{
    constructor() {
        super(options);
    }
    
    getByCode(code) {
        return this.select('*').where('code', '=', code).one();
    }

    incrementViews(id) {
        return this.db.query('UPDATE links SET clicks = clicks + 1 WHERE id = ?', id);
    }
    
    datatable(req, options = {users_id: null}) {
        return dataTableSearch.onRequest(req)
        .byFields(this.fields, async ({fields, length, order, search, start}) => {
            let countQuery = this.select(this.raw(`COUNT(id) as count`)).whereNull('deleted_at');
            let dataQuery = this.select('*').whereNull('deleted_at');

            countQuery = dataTableSearch.decorateSearch(countQuery, this.fields, search.data);
            dataQuery = dataTableSearch.decorateSearch(dataQuery, this.fields, search.data);

            if(options.users_id) {
                countQuery = countQuery.and('users_id', '=', options.users_id);
                dataQuery = dataQuery.and('users_id', '=', options.users_id);
            }

            const {count} = await countQuery.one();
                            
            const data = await dataQuery.orderBy(order.column, order.dir).limit(length, start).all();
            
            return {count, data};
        });
    }
}
module.exports = new Links();