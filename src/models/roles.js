const dataTableSearch = require('../helpers/datatable.js');
const BaseModel = require('./common/BaseModel.js');
const db = require('./common/database.js');

const options = {
    table_name: 'roles',
    connection: db,
    fields: [
        'name',
    ],
    control_fields: [
        'id',
        'created_at',
        'updated_at'
    ]
}

class Roles extends BaseModel{
    constructor() {
        super(options);
    }
    
    datatable(req) {
        return dataTableSearch
        .onRequest(req)
        .byFields(this.fields, async ({fields, length, order, search, start}) => {
            const {count} = await this.select(this.raw('COUNT(id) as count'))
            .where(search.column, 'LIKE', search.data+'%')
            .and('deleted_at', 'IS', this.raw('NULL'))
            .one();
                            
            const data = await this.select('*')
            .where(search.column, 'LIKE', search.data+'%')
            .and('deleted_at', 'IS', this.raw('NULL'))
            .orderBy(order.column, order.dir)
            .limit(length, start)
            .all();
            
            return {count, data};
        });
    }
}

module.exports = new Roles();