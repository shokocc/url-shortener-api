const dataTableSearch = require('../helpers/datatable.js');
const BaseModel = require('./common/BaseModel.js');
const db = require('./common/database.js');

const options = {
    table_name: 'users',
    connection: db,
    fields: [
        'username',
        'password',
        'roles_id',
        'status',
    ],
    control_fields: [
        'id',
        'created_at',
        'updated_at'
    ]
}

class Users extends BaseModel{
    constructor() {
        super(options);
    }

    getByUsername(username) {
        return this.select('*').where('username', '=', username).one();
    }
    
    datatable(req) {
        return dataTableSearch
        .onRequest(req)
        .byFields(this.fields, async ({fields, length, order, search, start}) => {
            const {count} = await this.select(this.raw('COUNT(id) as count'))
            .where(search.column, 'LIKE', search.data+'%')
            //.and('deleted_at', 'IS', this.raw('NULL'))
            .one();
                            
            const data = await this.select('*')
            .where(search.column, 'LIKE', search.data+'%')
            //.and('deleted_at', 'IS', this.raw('NULL'))
            .orderBy(order.column, order.dir)
            .limit(length, start)
            .all();
            
            return {count, data};
        });
    }
}

module.exports = new Users();