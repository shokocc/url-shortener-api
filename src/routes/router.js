const router = require('express').Router();

const expect = require('../middlewares/expect.js');
const {role, authorize} = require('../middlewares/oauth2.js');

const users = require('../controllers/users.js');
const links = require('../controllers/links.js');

// User
router.post('/api/users/register', expect({
    body: [
        'username',
        'password',
    ]
}), users.register);

router.post('/api/users/login', expect({ body: ['username','password'] }), users.login);

router.post('/api/users/token/status', expect({ body: ['access_token'] }), users.tokenStatus);

router.get('/api/users', authorize([role['Admin']]), users.getAll);
router.get('/api/users/datatable', authorize([role['Admin']]), users.getDataTable);
router.post('/api/users/status', authorize([role['Admin']]), expect({ body: ['id','status'] }), users.setStatus);

// Medical Specialities
router.get('/api/links/resolve/:code?', authorize(), expect({params: ['code']}), links.getByCode);

router.get('/api/users/links', authorize([role['User']]), links.getDataTable);
router.post('/api/users/links', authorize([role['User']]), expect({body: ['url']}), links.create);
router.put('/api/users/links', authorize([role['User']]), expect({body: ['id', 'status']}), links.update);
router.delete('/api/users/links', authorize([role['User']]), expect({body: ['id']}), links.delete);

module.exports = router;