// Load .env Enviroment Variables to process.env
require('mandatoryenv').load([
    'PORT',
    'SECRET',
    'BASE_URL',
    'DB_HOST',
    'DB_DATABASE',
    'DB_USER',
    'DB_PASSWORD'
]);

// Patch express in order to use async / await syntax
const {inject, errorHandler} = require('express-custom-error');
inject();

// Require Dependencies
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
const multer = require('multer');
const initLogger = require('whiz-logger');

initLogger({
    enableGlobalLogger: true,
    logDirectory: 'logs',
})

// Instantiate an Multer Uploader
const upload = multer();

// Instantiate an Express Application
const app = express();

// Configure Express App Instance
app.use(express.json());
app.use(express.urlencoded( { extended: true } ));
app.use(upload.array());

app.use(cookieParser());
app.use(cors());
app.use(helmet());

app.use(logger.middleware);

// Assign Routes
app.use('/', require('./routes/router.js'));

// Handle not valid route
app.use('*', (req, res) => {    
    res.status(404).json( {status: false, message: `Endpoint '${req.originalUrl.substring(0, 30)}...' Not Found`} );
})

// Handle errors
app.use((err, req, res, next) => {
    logger.error(err);
    errorHandler()(err, req, res, next);
}); // Respond to errors

// Open Server on selected Port
app.listen(env.PORT, () => logger.info('Server listening on port ', env.PORT));